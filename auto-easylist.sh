#!/bin/bash

function xecho ()
{
  echo $* | tee /dev/fd/3
}

exec 3>&1 1>auto-easylist.log 2>&1
fcount=0
xecho -e "\nAuto Easylist...\n"
for source in $(cat lists.txt); do
    xecho $source;
    fname=${source##*/}
    curl --silent $source >> ads.txt | tee /dev/fd/3
    xecho -e "\t`wc -l ads.txt | cut -d " " -f 1` lines downloaded"
    xecho -e "\nFiltering non-url content..."
    perl easylist.pl ads.txt > ads_parsed.txt | tee /dev/fd/3
    rm ads.txt | tee /dev/fd/3
    xecho -e "\t`wc -l ads_parsed.txt | cut -d " " -f 1` lines after parsing"
    xecho -e "\nRemoving duplicates..."
    sort -u ads_parsed.txt > ads_unique.txt | tee /dev/fd/3
    rm ads_parsed.txt | tee /dev/fd/3
    xecho -e "\t`wc -l ads_unique.txt | cut -d " " -f 1` lines after deduping"
    cat ads_unique.txt >> lists/all_lists.tmp | tee /dev/fd/3
    fname1=$(printf %02d $fcount)
    fname="${fname1}-${fname}"
    fcount=$((fcount + 1))
    cat ads_unique.txt > lists/$fname | tee /dev/fd/3
    rm ads_unique.txt | tee /dev/fd/3
done
xecho -e "\nProcessing all_lists.txt..."
sort -u lists/all_lists.tmp > lists/all_lists.txt | tee /dev/fd/3
rm lists/all_lists.tmp | tee /dev/fd/3
xecho "done..."
